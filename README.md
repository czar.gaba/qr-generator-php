# QR-generator-PHP

# Library Required 
- http://phpqrcode.sourceforge.net/index.php#demo

# Usage 
```
include('library/qrlib.php');

//declarations
$path = 'qr/'; // folder destination 
$qr= $path.uniqid().".png"; //saving of QR png in folder destination



// uniqid -> code generator for special id 



//usage
QRcode::png('Code Data',$qr,'H',10,3);
//Parameters ($string,$qr,ECC_LEVEL,pixel_side,frame_size) 
//ECC_LEVEL = L / M / Q / H
// Size 1-10
echo '<img src="'.$qr.'">';

```


# Author

<h3>Czar G.</h3> 
<h3>Head Technical | Mydevhouse | Freelance Web Developer</h3> 
<h3><a href="https://mydevhouse.dev">https://mydevhouse.dev</a></h3> <br>
<a href="https://www.facebook.com/appleideath" target="_blank"><img src="https://img.icons8.com/doodle/50/000000/facebook-new.png"></a>
<a href="https://www.reddit.com/r/appleideath" target="_blank"><img src="https://img.icons8.com/doodle/48/000000/reddit--v4.png"></a>
<a href="https://twitter.com/appleideathxx" target="_blank"><img src="https://img.icons8.com/cotton/48/000000/twitter.png"></a>
<a href="https://www.instagram.com/appleideath/" target="_blank"><img src="https://img.icons8.com/officel/64/000000/instagram-new.png"></a>
<a href="https://gitlab.com/czar.gaba" target="_blank"><img src="https://img.icons8.com/color/48/000000/gitlab.png"></a>
<a href="https://www.linkedin.com/in/czar-gaba-b67a24124/" target="_blank"><img src="https://img.icons8.com/doodle/48/000000/linkedin--v2.png"></a>

<p> Love is the Death of Duty</p>